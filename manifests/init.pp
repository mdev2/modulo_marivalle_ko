class modulo_examen{
class {'apache':}

apache::vhost { 'myMpwar.prod':
	port	=> '80',
	docroot	=> '/var/www/prod/',
	docroot_owner	=> 'vagrant',
	docroot_group => 'vagrant',
}

apache::vhost { 'myMpwar.dev':
	port	=> '80',
	docroot	=> '/var/www/dev/',
	docroot_owner	=> 'vagrant',
	docroot_group => 'vagrant',
}

file { "/var/www/prod/":
    ensure => "directory",
}
file { '/var/www/prod/index.php':
    ensure => file,
    replace => true,
    content  => "Hello World. Sistema operativo ${operatingsystem} ${operatingsystemrelease}",
    require => File ["/var/www/prod/"]
}

file { "/var/www/dev/":
    ensure => "directory",
}
file { '/var/www/dev/index.php':
    ensure => file,
    replace => true,
    content  => "<?php phpinfo();",
    require => File ["/var/www/dev/"]
}

}
